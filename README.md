# WebRTC Relay

This repository implements executables that simulate the behavior of a system where:

- A support vessel, at sea, is connected to the Internet through a high-latency,
  low-bandwidth VSAT connection and retrieves video from a submarine through
  a local interface.

- A relay machine, on land, is connected to the Internet through a low-latency,
  high bandwidth connection.

- A few (single-digit) viewers may want to watch the video stream concurrently.

  They may or may not have a good connection to the Internet.

Data transfer between the different components of the system is achieved through
the [WebRTC protocol].

[WebRTC protocol]: https://en.wikipedia.org/wiki/WebRTC

## Prerequisites

- Latest version of [GStreamer], main branch. Usage of the GStreamer development
  environment is recommended for testing on a Linux machine. The necessary
  development versions of dependencies need to be installed, these include but
  are not limited to libx264, openssl, ..

- The [gst-plugins-rs] sources

- The [rust compiler]

- [npm]

- [comcast] for testing congestion control

- <https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs/-/merge_requests/1245>
  must have been merged, otherwise edit Cargo.toml to point to a local version with
  the patch applied.

[GStreamer]: https://gitlab.freedesktop.org/gstreamer/gstreamer/
[gst-plugins-rs]: https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs/
[rust compiler]: https://www.rust-lang.org/learn/get-started
[npm]: https://www.npmjs.com/
[comcast]: https://github.com/tylertreat/comcast

## Running the signaller

From the root of `gst-plugins-rs`, run:

``` shell
$ WEBRTCSINK_SIGNALLING_SERVER_LOG=debug cargo run --bin gst-webrtc-signalling-server
```

Note the IP of this first machine, we will now refer to it as `$PRODUCER_IP`

## Exposing a web server to serve a web page to viewers

On the same machine, follow the instructions at
<https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs/-/blob/main/net/webrtc/gstwebrtc-api/README.md>

## Running the support vessel executable

On the same machine, from the root of this repository, run:

``` shell
GST_PLUGIN_FEATURE_RANK=x264enc:1000 cargo run --bin support-vessel -- "uridecodebin uri=file:///home/meh/Videos/diving_redux.webm"
```

This will serve a local file.

Alternatively, you could pass another bin description, for example:

```
GST_PLUGIN_FEATURE_RANK=x264enc:1000 cargo run --bin support-vessel -- "(v4l2src ! capsfilter caps=\"video/x-raw, framerate=30/1\")"
```

can serve the video from a webcam if one is available.

Look at the logs from the signalling server to determine the ID that was attributed
to this producer, for example:

```
2023-06-14T22:44:18.842127Z  INFO ThreadId(12) set_peer_status{peer_id="62a75d25-5333-411c-97b5-18f41286a7a9" status=PeerStatus { roles: [Producer], meta: None, peer_id: Some("62a75d25-5333-411c-97b5-18f41286a7a9") }}: gst_plugin_webrtc_signalling::handlers: registered as a producer peer_id=62a75d25-5333-411c-97b5-18f41286a7a9
```

## Running the land relay executable

Using the peer ID obtained at the previous step, and on the same machine, run:

``` shell
$ GST_PLUGIN_FEATURE_RANK=x264enc:1000 cargo run --bin land-relay -- "$PEER_ID"
```

This will expose a second producer, you can note its ID in the same manner.

Passing `--sfu` when running the land relay will cause it to strictly forward
incoming data without re-encoding it. This means very little extra processing
power will be used for each new viewer, but no congestion control will be
performed on the land-relay -> viewer leg of the system.

## Viewing the output from the land relay

On a different machine (at least if you want to test congestion control), open
a web browser (preferably chrome or firefox) and navigate to <https://$PRODUCER_IP:9090>.

Ignore the self-signed certificate, you should see a page with two producers listed.

Click on the one that matches the ID of the land relay, you should see a smooth
video stream with good bitrate, resolution and framerate.

## Testing congestion control

Finally, you can introduce congestion between the support vessel and the land relay
by running (on the first machine):

``` shell
/home/meh/go/bin/comcast --device lo --stop; /home/meh/go/bin/comcast --device=lo --target-bw 1000 --target-addr=$PRODUCER_IP --target-port=1:65535 --target-proto=udp
```

At this point you should see the video stream in the viewer freeze for a few
seconds, most likely display a few artefacts, but then eventually recover and keep
displaying a low-bitrate version of the video stream.

You can remove the congestion with:

```
/home/meh/go/bin/comcast --device lo --stop
```

and watch the quality of the video stream slowly recover.

This behavior should be observable with the land relay either in reencoding or sfu
mode.

The difference between either mode can be observed by introducing congestion between
the main machine and the viewer machine in a similar fashion, but that is left as an
exercise for the reader.
