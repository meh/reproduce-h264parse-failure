use anyhow::Error;

use clap::Parser;
use futures::{prelude::*, select_biased};
use gst::prelude::*;

#[derive(Parser, Debug)]
#[clap(about, version, author)]
/// Program arguments
struct Args {
    /// URI of file to serve. Must hold at least one audio and video stream
    source_description: String,
}

fn check_new_source_pad(sourcebin: &gst::Element, srcpad: &gst::Pad, sinkpad: &gst::Pad) {
    let caps = srcpad.query_caps(None);
    if let Some(s) = caps.structure(0) {
        if s.name().starts_with("video") {
            if !sinkpad.is_linked() {
                // TODO
                if let Err(err) = srcpad.link(sinkpad) {
                    sourcebin.post_error_message(gst::error_msg!(
                        gst::StreamError::Failed,
                        ["Failed to link sourcebin pad {err}"]
                    ));
                }
            }
        }
    }
}

async fn run(args: Args) -> Result<(), Error> {
    let pipeline = gst::Pipeline::new();

    let sourcebin = gst::parse::bin_from_description_full(
        &args.source_description,
        true,
        None,
        gst::ParseFlags::NO_SINGLE_ELEMENT_BINS,
    )?;
    let videoconvert = gst::ElementFactory::make("videoconvert").build()?;
    let videoscale = gst::ElementFactory::make("videoscale").build()?;
    let capsfilter = gst::ElementFactory::make("capsfilter").build()?;
    let webrtcsink = gst::ElementFactory::make("webrtcsink").build()?;

    pipeline.add_many([&sourcebin, &videoconvert, &videoscale, &capsfilter, &webrtcsink])?;

    gst::Element::link_many([&videoconvert, &videoscale, &capsfilter])?;

    videoscale.set_property_from_str("method", "lanczos");
    capsfilter.set_property_from_str("caps", "video/x-raw, width=1280, height=720");
    webrtcsink.set_property_from_str("video-caps", "video/x-h264");

    // We want to tweak how webrtcsink performs video scaling when needed, as
    // this can have a very visible impact over quality.
    //
    // To achieve that, we will connect to deep-element-added on the consumer
    // pipeline.
    webrtcsink.connect("consumer-pipeline-created", false, |values| {
        let pipeline = values[2].get::<gst::Pipeline>().unwrap();

        pipeline.connect("deep-element-added", false, |values| {
            let element = values[2].get::<gst::Element>().unwrap();

            if let Some(factory) = element.factory() {
                if factory.name().as_str() == "videoscale" {
                    element.set_property_from_str("method", "lanczos");
                }
            }

            None
        });

        None
    });

    // We *could* access the consumer encoder from our
    // consumer-pipeline-created handler, but doing so from an encoder-setup
    // callback is better practice, as it will also get called
    // when running the discovery pipelines, and changing properties on the
    // encoder may in theory affect the caps it outputs.
    webrtcsink.connect("encoder-setup", true, |values| {
        let encoder = values[3].get::<gst::Element>().unwrap();

        if let Some(factory) = encoder.factory() {
            match factory.name().as_str() {
                "x264enc" => {
                    encoder.set_property_from_str("speed-preset", "medium");
                }
                name => {
                    println!(
                        "Can't tune unsupported H264 encoder {name}, \
                             set GST_PLUGIN_FEATURE_RANK=x264enc:1000 when \
                             running the application"
                    );
                }
            }
        }

        Some(false.to_value())
    });

    capsfilter.link_pads(None, &webrtcsink, Some("video_0"))?;
    let sinkpad = videoconvert.static_pad("sink").unwrap();

    let sinkpad_clone = sinkpad.clone();
    let _ = sourcebin.iterate_src_pads().foreach(|pad| {
        check_new_source_pad(&sourcebin, &pad, &sinkpad_clone);
    });

    let sinkpad_clone = sinkpad.clone();
    sourcebin.connect_pad_added(move |bin, pad| {
        check_new_source_pad(&bin, &pad, &sinkpad_clone);
    });

    let sinkpad_clone = sinkpad.clone();
    sourcebin.connect_no_more_pads(move |bin| {
        if !sinkpad_clone.is_linked() {
            bin.post_error_message(gst::error_msg!(
                gst::StreamError::Failed,
                ["Could not find video stream in input file"]
            ));
        }
    });

    pipeline.set_state(gst::State::Playing)?;

    let mut bus_fut = pipeline.bus().unwrap().stream().fuse();

    loop {
        // Now simply loop until we either get interrupted or our pipeline
        // encounters an error or end of stream
        select_biased! {
            _ = tokio::signal::ctrl_c().fuse() => {
                eprintln!("Ctrl + C");
                break;
            }
            msg = bus_fut.next() => {
                if let Some(msg) = msg {
                    match msg.view() {
                        gst::MessageView::Eos(_) => {
                            eprintln!("End of stream");
                            break;
                        }
                        gst::MessageView::Error(err) => {
                            eprintln!("error: {}, details: {:?}", err.error(), err.debug());
                            pipeline.debug_to_dot_file(gst::DebugGraphDetails::all(), "support-vessel-error");
                            break;
                        }
                        gst::MessageView::Latency(_) => {
                            let _ = pipeline.recalculate_latency();
                        }
                        gst::MessageView::StateChanged(s) => {
                            pipeline.debug_to_dot_file(gst::DebugGraphDetails::all(), format!("support-vessel-{:?}-{:?}", s.old(), s.current()));
                        }
                        _ => ()
                    }
                } else {
                    break;
                }
            }
        }
    }

    pipeline
        .call_async_future(|pipeline| pipeline.set_state(gst::State::Null))
        .await?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    gst::init()?;

    gst_plugin_webrtc::plugin_register_static()?;
    gst_plugin_rtp::plugin_register_static()?;

    let args = Args::parse();

    run(args).await
}
