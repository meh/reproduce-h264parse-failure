use anyhow::Error;

use clap::Parser;
use futures::{prelude::*, select_biased};
use gst::prelude::*;

#[derive(Parser, Debug)]
#[clap(about, version, author)]
/// Program arguments
struct Args {
    /// Identifier of the support vessel
    producer_id: String,
    /// Act as a SFU instead of re-encoding one stream per final consumer
    #[arg(short, long)]
    sfu: bool,
}

fn check_new_source_pad(sourcebin: &gst::Element, srcpad: &gst::Pad, sinkpad: &gst::Pad) {
    if let Err(err) = srcpad.link(sinkpad) {
        sourcebin.post_error_message(gst::error_msg!(
            gst::StreamError::Failed,
            ["Failed to link sourcebin pad {err}"]
        ));
    }
}

async fn run(args: Args) -> Result<(), Error> {
    let pipeline = gst::Pipeline::new();

    let webrtcsrc = gst::ElementFactory::make("webrtcsrc").build()?;
    let capsfilter = gst::ElementFactory::make("capsfilter").build()?;
    let queue = gst::ElementFactory::make("queue").build()?;

    let webrtcsink = gst::ElementFactory::make("webrtcsink").build()?;

    pipeline.add_many([&webrtcsrc, &capsfilter, &queue, &webrtcsink])?;
    gst::Element::link_many([&capsfilter, &queue])?;

    if args.sfu {
        /* In forwarding mode, we want to work around webrtcsink complaining about
         * changes in codec_data, we do that by going to byte-stream and not letting
         * any other information filter through the caps
         */
        let depay = gst::ElementFactory::make("rtph264depay").build()?;
        let parse = gst::ElementFactory::make("h264parse").build()?;
        let h264_capsfilter = gst::ElementFactory::make("capsfilter").build()?;
        let capssetter = gst::ElementFactory::make("capssetter").build()?;

        pipeline.add_many([&depay, &parse, &h264_capsfilter, &capssetter])?;
        gst::Element::link_many([&queue, &depay, &parse, &h264_capsfilter, &capssetter])?;
        capssetter.link_pads(None, &webrtcsink, Some("video_0"))?;
        capsfilter.set_property_from_str("caps", "application/x-rtp");
        h264_capsfilter.set_property_from_str("caps", "video/x-h264, stream-format=byte-stream");
        capssetter.set_property_from_str("caps", "video/x-h264, stream-format=byte-stream");
        capssetter.set_property("replace", true);
    } else {
        /* When re-encoding, we make sure to keep the resolution and framerate
         * fixed so as to avoid webrtcsink erroring out because it could require
         * renegotiation
         */
        let videoconvert = gst::ElementFactory::make("videoconvert").build()?;
        let videoscale = gst::ElementFactory::make("videoscale").build()?;
        let videorate = gst::ElementFactory::make("videorate").build()?;
        let scale_filter = gst::ElementFactory::make("capsfilter").build()?;

        pipeline.add_many([&videoconvert, &videoscale, &videorate, &scale_filter])?;
        gst::Element::link_many([&queue, &videoconvert, &videoscale, &videorate, &scale_filter])?;
        scale_filter.link_pads(None, &webrtcsink, Some("video_0"))?;

        videoscale.set_property_from_str("method", "lanczos");
        capsfilter.set_property_from_str("caps", "video/x-raw");
        scale_filter.set_property_from_str("caps", "video/x-raw, width=1280, height=720, pixel-aspect-ratio=1/1, framerate=30/1");
    }

    let signaller = webrtcsrc.downcast_ref::<gst::Bin>().unwrap().child_by_name("signaller").unwrap();

    signaller.set_property("producer-peer-id", args.producer_id);
    webrtcsink.set_property_from_str("video-caps", "video/x-h264");

    webrtcsink.connect("encoder-setup", true, |values| {
        let encoder = values[3].get::<gst::Element>().unwrap();

        println!("Encoder: {}", encoder.factory().unwrap().name());

        if let Some(factory) = encoder.factory() {
            match factory.name().as_str() {
                "x264enc" => {
                    println!("Applying extra configuration to x264enc");
                    encoder.set_property_from_str("speed-preset", "medium");
                }
                name => {
                    println!(
                        "Can't tune unsupported H264 encoder {name}, \
                             set GST_PLUGIN_FEATURE_RANK=x264enc:1000 when \
                             running the application"
                    );
                }
            }
        }

        Some(false.to_value())
    });

    let sinkpad = capsfilter.static_pad("sink").unwrap();

    let sinkpad_clone = sinkpad.clone();
    webrtcsrc.connect_pad_added(move |bin, pad| {
        check_new_source_pad(&bin, &pad, &sinkpad_clone);
    });

    let sinkpad_clone = sinkpad.clone();
    webrtcsrc.connect_no_more_pads(move |bin| {
        if !sinkpad_clone.is_linked() {
            bin.post_error_message(gst::error_msg!(
                gst::StreamError::Failed,
                ["Could not find video stream in input stream"]
            ));
        }
    });

    pipeline.set_state(gst::State::Playing)?;

    let mut bus_fut = pipeline.bus().unwrap().stream().fuse();

    loop {
        // Now simply loop until we either get interrupted or our pipeline
        // encounters an error or end of stream
        select_biased! {
            _ = tokio::signal::ctrl_c().fuse() => {
                eprintln!("Ctrl + C");
                break;
            }
            msg = bus_fut.next() => {
                if let Some(msg) = msg {
                    match msg.view() {
                        gst::MessageView::Eos(_) => {
                            eprintln!("End of stream");
                            break;
                        }
                        gst::MessageView::Error(err) => {
                            eprintln!("error: {}, details: {:?}", err.error(), err.debug());
                            pipeline.debug_to_dot_file(gst::DebugGraphDetails::all(), "land-relay-error");
                            break;
                        }
                        gst::MessageView::Latency(_) => {
                            let _ = pipeline.recalculate_latency();
                        }
                        gst::MessageView::StateChanged(s) => {
                            pipeline.debug_to_dot_file(gst::DebugGraphDetails::all(), format!("land-relay-{:?}-{:?}", s.old(), s.current()));
                        }
                        _ => ()
                    }
                } else {
                    break;
                }
            }
        }
    }

    pipeline
        .call_async_future(|pipeline| pipeline.set_state(gst::State::Null))
        .await?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    gst::init()?;

    gst_plugin_webrtc::plugin_register_static()?;
    gst_plugin_rtp::plugin_register_static()?;

    let args = Args::parse();

    run(args).await
}
